package be.hics.sandbox.pricewar;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class RandomWar {

    public static BigDecimal[][] randomDemand(final int size, final double quantityMin, final double quantityMax, final double priceMin, final double priceMax) {
        return randomPriceStream(size, priceMin, priceMax).map(price -> RandomWar.mapQuantityToPrice(quantityMin, quantityMax, price)).toArray(BigDecimal[][]::new);
    }

    public static BigDecimal[][] randomSupply(final int size, final double quantityMin, final double quantityMax, final double priceMin, final double priceMax) {
        return randomPriceStream(size, priceMin, priceMax).distinct().map(price -> RandomWar.mapQuantityToPrice(quantityMin, quantityMax, price)).toArray(BigDecimal[][]::new);
    }

    private static Stream<BigDecimal> randomPriceStream(final int size, final double priceMin, final double priceMax) {
        return IntStream.range(0, new Random().nextInt(size) + 1).mapToObj(i -> randomBigDecimal(priceMin, priceMax));
    }

    private static BigDecimal[] mapQuantityToPrice(final double quantityMin, final double quantityMax, final BigDecimal price) {
        BigDecimal[] result = new BigDecimal[2];
        result[0] = randomBigDecimal(quantityMin, quantityMax);
        result[1] = price;
        return result;
    }

    public static BigDecimal randomBigDecimal(final double min, final double max) {
        return new BigDecimal(min + new Random().nextDouble() * (max - min)).setScale(2, RoundingMode.HALF_UP);
    }
}
