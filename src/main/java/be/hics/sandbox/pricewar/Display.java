package be.hics.sandbox.pricewar;

import java.math.BigDecimal;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Display {

    public static String print(final BigDecimal[][] array) {
        return "{" + Stream.of(array).map(Display::printPair).collect(Collectors.joining(", ")) + "]";
    }

    public static String printPair(final BigDecimal[] array) {
        return String.format("[%s, %s]", array[0], array[1]);
    }

}
