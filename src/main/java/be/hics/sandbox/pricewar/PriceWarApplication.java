package be.hics.sandbox.pricewar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.function.Function;
import java.util.stream.Stream;

@SpringBootApplication
public class PriceWarApplication {

    public static void main(String[] args) {
        SpringApplication.run(PriceWarApplication.class, args);

        doIt("demand:10 / supply:25 / std:5.0 (1)",
                RandomWar.randomDemand(10, 100, 2000, 25.00, 30.00),
                RandomWar.randomSupply(25, 1000, 5000, 20.00, 32.50),
                RandomWar.randomBigDecimal(2500, 10000));
        doIt("demand:10 / supply:25 / std:5.0 (2)",
                RandomWar.randomDemand(10, 100, 2000, 25.00, 30.00),
                RandomWar.randomSupply(25, 1000, 5000, 20.00, 32.50),
                RandomWar.randomBigDecimal(2500, 10000));
        doIt("demand:10 / supply:25 / std:5.0 (3)",
                RandomWar.randomDemand(10, 100, 2000, 25.00, 30.00),
                RandomWar.randomSupply(25, 1000, 5000, 20.00, 32.50),
                RandomWar.randomBigDecimal(2500, 10000));
        doIt("demand:10 / supply:25 / std:5.0 (4)",
                RandomWar.randomDemand(10, 100, 2000, 25.00, 30.00),
                RandomWar.randomSupply(25, 1000, 5000, 20.00, 32.50),
                RandomWar.randomBigDecimal(2500, 10000));
        doIt("demand:10 / supply:25 / std:5.0 (5)",
                RandomWar.randomDemand(10, 100, 2000, 25.00, 30.00),
                RandomWar.randomSupply(25, 1000, 5000, 20.00, 32.50),
                RandomWar.randomBigDecimal(2500, 10000));

        doIt("demand:50 / supply:50 / std:1.0 (1)",
                RandomWar.randomDemand(50, 100, 150, 0.5, 1.6),
                RandomWar.randomSupply(50, 250, 500, 0.33, 1.75),
                RandomWar.randomBigDecimal(250, 500));
        doIt("demand:50 / supply:50 / std:1.0 (2)",
                RandomWar.randomDemand(50, 100, 150, 0.5, 1.6),
                RandomWar.randomSupply(50, 250, 500, 0.33, 1.75),
                RandomWar.randomBigDecimal(250, 500));
        doIt("demand:50 / supply:50 / std:1.0 (3)",
                RandomWar.randomDemand(50, 100, 150, 0.5, 1.6),
                RandomWar.randomSupply(50, 250, 500, 0.33, 1.75),
                RandomWar.randomBigDecimal(250, 500));
        doIt("demand:50 / supply:50 / std:1.0 (4)",
                RandomWar.randomDemand(50, 100, 150, 0.5, 1.6),
                RandomWar.randomSupply(50, 250, 500, 0.33, 1.75),
                RandomWar.randomBigDecimal(250, 500));
        doIt("demand:50 / supply:50 / std:1.0 (5)",
                RandomWar.randomDemand(50, 100, 150, 0.5, 1.6),
                RandomWar.randomSupply(50, 250, 500, 0.33, 1.75),
                RandomWar.randomBigDecimal(250, 500));

        doIt("demand:100 / supply:500 / std:100.0 (1)",
                RandomWar.randomDemand(100, 50, 2500, 800.00, 1250.00),
                RandomWar.randomSupply(500, 2500, 50000, 500.00, 1500.00),
                RandomWar.randomBigDecimal(2500, 50000));
        doIt("demand:100 / supply:500 / std:100.0 (2)",
                RandomWar.randomDemand(100, 50, 2500, 800.00, 1250.00),
                RandomWar.randomSupply(500, 2500, 50000, 500.00, 1500.00),
                RandomWar.randomBigDecimal(2500, 50000));
        doIt("demand:100 / supply:500 / std:100.0 (3)",
                RandomWar.randomDemand(100, 50, 2500, 800.00, 1250.00),
                RandomWar.randomSupply(500, 2500, 50000, 500.00, 1500.00),
                RandomWar.randomBigDecimal(2500, 50000));
        doIt("demand:100 / supply:500 / std:100.0 (4)",
                RandomWar.randomDemand(100, 50, 2500, 800.00, 1250.00),
                RandomWar.randomSupply(500, 2500, 50000, 500.00, 1500.00),
                RandomWar.randomBigDecimal(2500, 50000));
        doIt("demand:100 / supply:500 / std:100.0 (5)",
                RandomWar.randomDemand(100, 50, 2500, 800.00, 1250.00),
                RandomWar.randomSupply(500, 2500, 50000, 500.00, 1500.00),
                RandomWar.randomBigDecimal(2500, 50000));

        doIt("demand:1000 / supply:5000 / std:10.0 (1)",
                RandomWar.randomDemand(1000, 50, 100, 75.00, 85.00),
                RandomWar.randomSupply(5000, 250, 1000, 70.00, 90.00),
                RandomWar.randomBigDecimal(250, 1000));
        doIt("demand:1000 / supply:5000 / std:10.0 (2)",
                RandomWar.randomDemand(1000, 50, 100, 75.00, 85.00),
                RandomWar.randomSupply(5000, 250, 1000, 70.00, 90.00),
                RandomWar.randomBigDecimal(250, 1000));
    }

    private static void doIt(final String title, final BigDecimal[][] demand, final BigDecimal[][] supply, final BigDecimal capacity) {
        System.out.println("----- " + title + " -----");
        System.out.println("DEMAND: " + Display.print(demand) + " --- SUPPLY: " + Display.print(supply) + " --- CAPACITY: " + capacity.toString());
        System.out.println("===> PRICE: " + PriceWar.pickYourPrice(demand, supply, capacity));
    }

}
