package be.hics.sandbox.pricewar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PriceWar {

    private static QP toQP(final BigDecimal[] qp) {
        if (qp.length != 2)
            throw new RuntimeException("Invalid QP array.");
        return new QP(qp[0], qp[1]);
    }

    private static BigDecimal cloneBigDecimal(final BigDecimal bd) {
        return new BigDecimal(bd.toString());
    }

    private static QP cloneQP(final QP qp) {
        return new QP(cloneBigDecimal(qp.getQuantity()), cloneBigDecimal(qp.getPrice()));
    }

    private static QP cloneQP(final BigDecimal quantity, final BigDecimal price) {
        return new QP(cloneBigDecimal(quantity), cloneBigDecimal(price));
    }

    private static List<QP> cloneList(final List<QP> list) {
        return list.stream().map(PriceWar::cloneQP).collect(Collectors.toList());
    }

    private static List<QP> cloneListAddAndResort(final List<QP> list, final QP qp, final Comparator<QP> comparator) {
        List<QP> newList = list.stream().map(PriceWar::cloneQP).collect(Collectors.toList());
        newList.add(qp);
        return newList.stream().sorted(comparator).collect(Collectors.toList());
    }

    private static List<BigDecimal> getValidPrices(final List<QP> demand, final List<QP> supply) {
        List<BigDecimal> otherSupplyPrices = supply.stream().map(QP::getPrice).collect(Collectors.toList());
        BigDecimal step = new BigDecimal("0.01");
        Stream<BigDecimal> myDemandPrices = demand.stream().map(QP::getPrice);
        Function<QP, BigDecimal> nextValidPrice = qp -> {
            BigDecimal next = qp.getPrice().subtract(step);
            while (otherSupplyPrices.contains(next) && next.compareTo(BigDecimal.ZERO) > 0) {
                next = next.subtract(step);
            }
            return next;
        };
        Stream<BigDecimal> mySupplyPrices = supply.stream().map(nextValidPrice);
        return Stream.concat(myDemandPrices, mySupplyPrices).distinct().sorted(BigDecimal::compareTo).filter(bd -> bd.compareTo(BigDecimal.ZERO) > 0).collect(Collectors.toList());
    }

    private static Optional<QP> getRevenue(final List<QP> demand, final List<QP> supply, final BigDecimal price) {
        List<QP> remainingDemand = cloneList(demand);
        List<QP> remainingSupply = cloneList(supply);
        List<QP> usedSupply = new ArrayList<>();
        usedSupply.addAll(supply.stream().map(qp -> new QP(BigDecimal.ZERO, cloneBigDecimal(qp.getPrice()))).collect(Collectors.toList()));
        for (int d = 0; d < remainingDemand.size(); d++) {
            QP qpD = remainingDemand.get(d);
            for (int s = 0; s < remainingSupply.size() && qpD.getQuantity().compareTo(BigDecimal.ZERO) > 0; s++) {
                QP qpS = remainingSupply.get(s);
                QP qpU = usedSupply.get(s);
                if (qpS.getPrice().compareTo(qpD.getPrice()) <= 0 && qpS.getQuantity().compareTo(BigDecimal.ZERO) > 0) {
                    BigDecimal min = qpD.getQuantity().min(qpS.getQuantity());
                    qpD.setQuantity(qpD.getQuantity().subtract(min));
                    remainingDemand.set(d, qpD);
                    qpS.setQuantity(qpS.getQuantity().subtract(min));
                    remainingSupply.set(s, qpS);
                    qpU.setQuantity(qpU.getQuantity().add(min));
                    usedSupply.set(s, qpU);
                }
            }
        }
        Function<List<QP>, List<QP>> filter = l -> l.stream().filter(qp -> qp.getPrice().compareTo(price) == 0 && qp.getQuantity().compareTo(BigDecimal.ZERO) > 0).collect(Collectors.toList());
        return filter.apply(usedSupply).stream().sorted(Comparator.comparing(QP::getRevenue).reversed()).findFirst();
    }

    public static BigDecimal pickYourPrice(final BigDecimal[][] demand, final BigDecimal[][] supply, final BigDecimal capacity) {
        Function<BigDecimal[][], List<QP>> toQPList = qp -> Stream.of(qp).map(PriceWar::toQP).collect(Collectors.toList());
        return pickYourPrice(toQPList.apply(demand), toQPList.apply(supply), capacity);
    }

    private static BigDecimal pickYourPrice(final List<QP> demand, final List<QP> supply, final BigDecimal capacity) {
        List<QP> sortedSupply = supply.stream().sorted(Comparator.comparing(QP::getPrice)).collect(Collectors.toList());
        List<BigDecimal> myValidPrices = getValidPrices(demand, sortedSupply);
        Function<BigDecimal, Optional<QP>> toRevenue = p -> getRevenue(demand, cloneListAddAndResort(sortedSupply, cloneQP(capacity, p), Comparator.comparing(QP::getPrice)), p);
        List<QP> myRevenues = myValidPrices.stream().map(toRevenue).filter(r -> r.isPresent()).map(r -> r.get())
                .sorted(Comparator.comparing(QP::getRevenue).thenComparing(QP::getPrice).reversed())
                .collect(Collectors.toList());
        Optional<QP> qp = myRevenues.stream().findFirst();
        return qp.isPresent() ? qp.get().getPrice() : BigDecimal.ZERO;
    }

    private static class QP {
        private BigDecimal q;
        private BigDecimal p;

        public QP(final BigDecimal q, final BigDecimal p) {
            this.q = q;
            this.p = p;
        }

        public BigDecimal getQuantity() {
            return q;
        }

        public void setQuantity(BigDecimal q) {
            this.q = q;
        }

        public BigDecimal getPrice() {
            return p;
        }

        public BigDecimal getRevenue() {
            return q.multiply(p);
        }

        @Override
        public String toString() {
            return String.format("[%s, %s]", q.toString(), p.toString());
        }
    }

}
