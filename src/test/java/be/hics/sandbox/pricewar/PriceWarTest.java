package be.hics.sandbox.pricewar;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.function.Function;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.comparesEqualTo;

public class PriceWarTest {

    private static Function<String, BigDecimal> s2d = s -> new BigDecimal(s);
    private static Function<String[], BigDecimal[]> s2d2 = sArr -> Stream.of(sArr).map(s2d).toArray(BigDecimal[]::new);
    private static Function<String[][], BigDecimal[][]> s2d3 = sArrArr -> Stream.of(sArrArr).map(s2d2).toArray(BigDecimal[][]::new);

    private void pickYourPrice(final String[][] demand, final String[][] supply, final String capacity, final String expected) {
        BigDecimal result = PriceWar.pickYourPrice(s2d3.apply(demand), s2d3.apply(supply), s2d.apply(capacity));
        assertThat(result, is(comparesEqualTo(new BigDecimal(expected))));
    }

    @Test
    public void inBetweenTwoCompetitors() {
        String[][] demand = {{"1100", "25.13"}, {"1000", "23.10"}, {"888", "19.19"}, {"3000", "11.80"}};
        String[][] supply = {{"5000", "17.00"}, {"2000", "16.00"}};
        pickYourPrice(demand, supply, "100", "16.99");
    }

    @Test
    public void lowerPriceWithHighQuantity() {
        String[][] demand = {{"900", "10.00"}, {"200", "11.00"}, {"300", "12.00"}, {"400", "13.00"}};
        String[][] supply = {{"300", "11.50"}, {"300", "12.50"}};
        pickYourPrice(demand, supply, "1000", "10.00");
    }

    @Test
    public void onlyOneDemand() {
        String[][] demand = {{"100", "10.00"}};
        String[][] supply = {{"50", "9.99"}, {"50", "9.98"}};
        pickYourPrice(demand, supply, "50", "9.97");
    }

    @Test
    public void noSolution() {
        String[][] demand = {{"100", "10.00"}};
        String[][] supply = {{"50", "14.00"}, {"50", "0.40"}, {"50", "15.00"}, {"50", "0.20"}, {"50", "0.10"}, {"50", "0.02"}, {"50", "0.01"}, {"50", "0.03"}};
        pickYourPrice(demand, supply, "1000", "0.00");
    }

    @Test
    public void noDemand() {
        String[][] demand = {};
        String[][] supply = {{"50", "14.00"}, {"50", "0.40"}, {"50", "15.00"}, {"50", "0.20"}, {"50", "0.10"}, {"50", "0.02"}, {"50", "0.01"}, {"50", "0.03"}};
        pickYourPrice(demand, supply, "1000", "0.00");
    }

    @Test
    public void noCompetition() {
        String[][] demand = {{"900", "10.00"}, {"200", "11.00"}, {"300", "12.00"}, {"400", "13.00"}};
        String[][] supply = {};
        pickYourPrice(demand, supply, "100", "13.00");
    }

    @Test
    public void sameResultWithDifferentPrices() {
        String[][] demand = {{"75", "15.00"}, {"50", "16.00"}, {"16", "25"}};
        String[][] supply = {{"100", "12.50"}};
        pickYourPrice(demand, supply, "25", "25.00");
    }

    @Test
    public void noCapacity() {
        String[][] demand = {{"500", "35.00"}, {"1000", "33.33"}, {"750", "39"}};
        String[][] supply = {{"10000", "34.50"}, {"9000", "30.50"}};
        pickYourPrice(demand, supply, "0", "0.00");
    }

    @Test
    public void smallDemandsLargeCapacities() {
        String[][] demand = {{"50", "7.00"}, {"100", "9.99"}, {"75", "8.00"}, {"115", "7.99"}, {"150", "8.10"}, {"90", "11.00"}, {"200", "7.5"}, {"150", "9.99"}, {"250", "8.25"}, {"300", "7.50"}};
        String[][] supply = {{"300", "8.5"}, {"500", "9.0"}, {"200", "7.00"}};
        pickYourPrice(demand, supply, "500", "8.10");
    }

    @Test
    public void unlimitedCapacity() {
        String[][] demand = {{"100", "20.00"}, {"50", "30.00"}, {"300", "5.00"}, {"5000", "25.00"}};
        String[][] supply = {{"1000", "27"}, {"500", "16"}};
        pickYourPrice(demand, supply, "1000000", "25.00");
    }

}
