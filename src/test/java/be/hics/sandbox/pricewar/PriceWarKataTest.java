package be.hics.sandbox.pricewar;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.Assert.assertTrue;

public class PriceWarKataTest {

    private static Function<String, BigDecimal> s2d = s -> new BigDecimal(s);
    private static Function<String[], BigDecimal[]> s2d2 = sArr -> Stream.of(sArr).map(s2d).toArray(BigDecimal[]::new);
    private static Function<String[][], BigDecimal[][]> s2d3 = sArrArr -> Stream.of(sArrArr).map(s2d2).toArray(BigDecimal[][]::new);

    private void pickYourPrice(final String[][] demand, final String[][] supply, final String capacity, final String expected) {
        pickYourPrice(s2d3.apply(demand), s2d3.apply(supply), s2d.apply(capacity), s2d.apply(expected));
    }

    private void pickYourPrice(final BigDecimal[][] demand, final BigDecimal[][] supply, final BigDecimal capacity, final BigDecimal expected) {
        BigDecimal result = PriceWar.pickYourPrice(demand, supply, capacity);
        assertTrue("Returned price doesn't match expected value.", result.compareTo(expected) == 0);
    }

    @Test
    public void inBetweenTwoCompetitors() {
        String[][] demand = {{"1100", "25.13"}, {"1000", "23.10"}, {"888", "19.19"}, {"3000", "11.80"}};
        String[][] supply = {{"5000", "17.00"}, {"2000", "16.00"}};
        pickYourPrice(demand, supply, "100", "16.99");
    }

    @Test
    public void lowerPriceWithHighQuantity() {
        String[][] demand = {{"900", "10.00"}, {"200", "11.00"}, {"300", "12.00"}, {"400", "13.00"}};
        String[][] supply = {{"300", "11.50"}, {"300", "12.50"}};
        pickYourPrice(demand, supply, "1000", "10.00");
    }

    @Test
    public void onlyOneDemand() {
        String[][] demand = {{"100", "10.00"}};
        String[][] supply = {{"50", "9.99"}, {"50", "9.98"}};
        pickYourPrice(demand, supply, "50", "9.97");
    }

    @Test
    public void noSolution() {
        String[][] demand = {{"100", "10.00"}};
        String[][] supply = {{"50", "14.00"}, {"50", "0.40"}, {"50", "15.00"}, {"50", "0.20"}, {"50", "0.10"}, {"50", "0.02"}, {"50", "0.01"}, {"50", "0.03"}};
        pickYourPrice(demand, supply, "1000", "0.00");
    }

    @Test
    public void noDemand() {
        String[][] demand = {};
        String[][] supply = {{"50", "14.00"}, {"50", "0.40"}, {"50", "15.00"}, {"50", "0.20"}, {"50", "0.10"}, {"50", "0.02"}, {"50", "0.01"}, {"50", "0.03"}};
        pickYourPrice(demand, supply, "1000", "0.00");
    }

    @Test
    public void noCompetition() {
        String[][] demand = {{"900", "10.00"}, {"200", "11.00"}, {"300", "12.00"}, {"400", "13.00"}};
        String[][] supply = {};
        pickYourPrice(demand, supply, "100", "13.00");
    }

    @Test
    public void sameResultWithDifferentPrices() {
        String[][] demand = {{"75", "15.00"}, {"50", "16.00"}, {"16", "25"}};
        String[][] supply = {{"100", "12.50"}};
        pickYourPrice(demand, supply, "25", "25.00");
    }

    @Test
    public void noCapacity() {
        String[][] demand = {{"500", "35.00"}, {"1000", "33.33"}, {"750", "39"}};
        String[][] supply = {{"10000", "34.50"}, {"9000", "30.50"}};
        pickYourPrice(demand, supply, "0", "0.00");
    }

    @Test
    public void smallDemandsLargeCapacities() {
        String[][] demand = {{"50", "7.00"}, {"100", "9.99"}, {"75", "8.00"}, {"115", "7.99"}, {"150", "8.10"}, {"90", "11.00"}, {"200", "7.5"}, {"150", "9.99"}, {"250", "8.25"}, {"300", "7.50"}};
        String[][] supply = {{"300", "8.5"}, {"500", "9.0"}, {"200", "7.00"}};
        pickYourPrice(demand, supply, "500", "8.10");
    }

    @Test
    public void unlimitedCapacity() {
        String[][] demand = {{"100", "20.00"}, {"50", "30.00"}, {"300", "5.00"}, {"5000", "25.00"}};
        String[][] supply = {{"1000", "27"}, {"500", "16"}};
        pickYourPrice(demand, supply, "1000000", "25.00");
    }

    // ----- RANDOM -----

    private void random1() {
        BigDecimal[][] demand = RandomWarGenerator.randomDemand(10, 100, 2000, 25.00, 30.00);
        BigDecimal[][] supply = RandomWarGenerator.randomSupply(25, 1000, 5000, 20.00, 32.50);
        BigDecimal capacity = RandomWarGenerator.randomBigDecimal(2500, 10000);
        BigDecimal expected = RandomWarTester.pickYourPrice(demand, supply, capacity);
        pickYourPrice(demand, supply, capacity, expected);
    }

    @Test
    public void saltySpace() {
        random1();
    }

    @Test
    public void aliveKnife() {
        random1();
    }

    @Test
    public void plateGuitar() {
        random1();
    }

    @Test
    public void toysTable() {
        random1();
    }

    @Test
    public void begWren() {
        random1();
    }

    private void random2() {
        BigDecimal[][] demand = RandomWarGenerator.randomDemand(50, 100, 150, 0.5, 1.6);
        BigDecimal[][] supply = RandomWarGenerator.randomSupply(50, 250, 500, 0.33, 1.75);
        BigDecimal capacity = RandomWarGenerator.randomBigDecimal(250, 500);
        BigDecimal expected = RandomWarTester.pickYourPrice(demand, supply, capacity);
        pickYourPrice(demand, supply, capacity, expected);
    }

    @Test
    public void watchWealth() {
        random2();
    }

    @Test
    public void weatherShip() {
        random2();
    }

    @Test
    public void tarzanToast() {
        random2();
    }

    @Test
    public void vulgarRoof() {
        random2();
    }

    @Test
    public void softSwing() {
        random2();
    }

    private void random3() {
        BigDecimal[][] demand = RandomWarGenerator.randomDemand(100, 50, 2500, 800.00, 1250.00);
        BigDecimal[][] supply = RandomWarGenerator.randomSupply(500, 2500, 50000, 500.00, 1500.00);
        BigDecimal capacity = RandomWarGenerator.randomBigDecimal(2500, 50000);
        BigDecimal expected = RandomWarTester.pickYourPrice(demand, supply, capacity);
        pickYourPrice(demand, supply, capacity, expected);
    }

    @Test
    public void lettuceDay() {
        random3();
    }

    @Test
    public void cattleCrack() {
        random3();
    }

    @Test
    public void loveMask() {
        random3();
    }

    @Test
    public void procurePancake() {
        random3();
    }

    @Test
    public void cricketMister() {
        random3();
    }

    private void random4() {
        BigDecimal[][] demand = RandomWarGenerator.randomDemand(1000, 50, 100, 75.00, 85.00);
        BigDecimal[][] supply = RandomWarGenerator.randomSupply(5000, 250, 1000, 70.00, 90.00);
        BigDecimal capacity = RandomWarGenerator.randomBigDecimal(250, 1000);
        BigDecimal expected = RandomWarTester.pickYourPrice(demand, supply, capacity);
        pickYourPrice(demand, supply, capacity, expected);
    }

    @Test
    public void magnumMicro() {
        random4();
    }

    @Test
    public void discoDispute() {
        random4();
    }

    @Test
    public void scrollSphere() {
        random4();
    }

    private static class RandomWarGenerator {

        public static BigDecimal[][] randomDemand(final int size, final double quantityMin, final double quantityMax, final double priceMin, final double priceMax) {
            return randomPriceStream(size, priceMin, priceMax).map(price -> RandomWarGenerator.mapQuantityToPrice(quantityMin, quantityMax, price)).toArray(BigDecimal[][]::new);
        }

        public static BigDecimal[][] randomSupply(final int size, final double quantityMin, final double quantityMax, final double priceMin, final double priceMax) {
            return randomPriceStream(size, priceMin, priceMax).distinct().map(price -> RandomWarGenerator.mapQuantityToPrice(quantityMin, quantityMax, price)).toArray(BigDecimal[][]::new);
        }

        private static Stream<BigDecimal> randomPriceStream(final int size, final double priceMin, final double priceMax) {
            return IntStream.range(0, new Random().nextInt(size) + 1).mapToObj(i -> randomBigDecimal(priceMin, priceMax));
        }

        private static BigDecimal[] mapQuantityToPrice(final double quantityMin, final double quantityMax, final BigDecimal price) {
            BigDecimal[] result = new BigDecimal[2];
            result[0] = randomBigDecimal(quantityMin, quantityMax);
            result[1] = price;
            return result;
        }

        public static BigDecimal randomBigDecimal(final double min, final double max) {
            return new BigDecimal(min + new Random().nextDouble() * (max - min)).setScale(2, RoundingMode.HALF_UP);
        }
    }

    private static class RandomWarTester {

        private static QP toQP(final BigDecimal[] qp) {
            if (qp.length != 2)
                throw new RuntimeException("Invalid QP array.");
            return new QP(qp[0], qp[1]);
        }

        private static BigDecimal cloneBigDecimal(final BigDecimal bd) {
            return new BigDecimal(bd.toString());
        }

        private static QP cloneQP(final QP qp) {
            return new QP(cloneBigDecimal(qp.getQuantity()), cloneBigDecimal(qp.getPrice()));
        }

        private static QP cloneQP(final BigDecimal quantity, final BigDecimal price) {
            return new QP(cloneBigDecimal(quantity), cloneBigDecimal(price));
        }

        private static List<QP> cloneList(final List<QP> list) {
            return list.stream().map(RandomWarTester::cloneQP).collect(Collectors.toList());
        }

        private static List<QP> cloneListAddAndResort(final List<QP> list, final QP qp, final Comparator<QP> comparator) {
            List<QP> newList = list.stream().map(RandomWarTester::cloneQP).collect(Collectors.toList());
            newList.add(qp);
            return newList.stream().sorted(comparator).collect(Collectors.toList());
        }

        private static List<BigDecimal> getValidPrices(final List<QP> demand, final List<QP> supply) {
            List<BigDecimal> otherSupplyPrices = supply.stream().map(QP::getPrice).collect(Collectors.toList());
            BigDecimal step = new BigDecimal("0.01");
            Stream<BigDecimal> myDemandPrices = demand.stream().map(QP::getPrice);
            Function<QP, BigDecimal> nextValidPrice = qp -> {
                BigDecimal next = qp.getPrice().subtract(step);
                while (otherSupplyPrices.contains(next) && next.compareTo(BigDecimal.ZERO) > 0) {
                    next = next.subtract(step);
                }
                return next;
            };
            Stream<BigDecimal> mySupplyPrices = supply.stream().map(nextValidPrice);
            return Stream.concat(myDemandPrices, mySupplyPrices).distinct().sorted(BigDecimal::compareTo).filter(bd -> bd.compareTo(BigDecimal.ZERO) > 0).collect(Collectors.toList());
        }

        private static Optional<QP> getRevenue(final List<QP> demand, final List<QP> supply, final BigDecimal price) {
            List<QP> remainingDemand = cloneList(demand);
            List<QP> remainingSupply = cloneList(supply);
            List<QP> usedSupply = new ArrayList<>();
            usedSupply.addAll(supply.stream().map(qp -> new QP(BigDecimal.ZERO, cloneBigDecimal(qp.getPrice()))).collect(Collectors.toList()));
            for (int d = 0; d < remainingDemand.size(); d++) {
                QP qpD = remainingDemand.get(d);
                for (int s = 0; s < remainingSupply.size() && qpD.getQuantity().compareTo(BigDecimal.ZERO) > 0; s++) {
                    QP qpS = remainingSupply.get(s);
                    QP qpU = usedSupply.get(s);
                    if (qpS.getPrice().compareTo(qpD.getPrice()) <= 0 && qpS.getQuantity().compareTo(BigDecimal.ZERO) > 0) {
                        BigDecimal min = qpD.getQuantity().min(qpS.getQuantity());
                        qpD.setQuantity(qpD.getQuantity().subtract(min));
                        remainingDemand.set(d, qpD);
                        qpS.setQuantity(qpS.getQuantity().subtract(min));
                        remainingSupply.set(s, qpS);
                        qpU.setQuantity(qpU.getQuantity().add(min));
                        usedSupply.set(s, qpU);
                    }
                }
            }
            Function<List<QP>, List<QP>> filter = l -> l.stream().filter(qp -> qp.getPrice().compareTo(price) == 0 && qp.getQuantity().compareTo(BigDecimal.ZERO) > 0).collect(Collectors.toList());
            return filter.apply(usedSupply).stream().sorted(Comparator.comparing(QP::getRevenue).reversed()).findFirst();
        }

        public static BigDecimal pickYourPrice(final BigDecimal[][] demand, final BigDecimal[][] supply, final BigDecimal capacity) {
            Function<BigDecimal[][], List<QP>> toQPList = qp -> Stream.of(qp).map(RandomWarTester::toQP).collect(Collectors.toList());
            return pickYourPrice(toQPList.apply(demand), toQPList.apply(supply), capacity);
        }

        private static BigDecimal pickYourPrice(final List<QP> demand, final List<QP> supply, final BigDecimal capacity) {
            List<QP> sortedSupply = supply.stream().sorted(Comparator.comparing(QP::getPrice)).collect(Collectors.toList());
            List<BigDecimal> myValidPrices = getValidPrices(demand, sortedSupply);
            Function<BigDecimal, Optional<QP>> toRevenue = p -> getRevenue(demand, cloneListAddAndResort(sortedSupply, cloneQP(capacity, p), Comparator.comparing(QP::getPrice)), p);
            List<QP> myRevenues = myValidPrices.stream().map(toRevenue).filter(r -> r.isPresent()).map(r -> r.get())
                    .sorted(Comparator.comparing(QP::getRevenue).thenComparing(QP::getPrice).reversed())
                    .collect(Collectors.toList());
            Optional<QP> qp = myRevenues.stream().findFirst();
            return qp.isPresent() ? qp.get().getPrice() : BigDecimal.ZERO;
        }

        private static class QP {
            private BigDecimal q;
            private BigDecimal p;

            public QP(final BigDecimal q, final BigDecimal p) {
                this.q = q;
                this.p = p;
            }

            public BigDecimal getQuantity() {
                return q;
            }

            public void setQuantity(BigDecimal q) {
                this.q = q;
            }

            public BigDecimal getPrice() {
                return p;
            }

            public BigDecimal getRevenue() {
                return q.multiply(p);
            }

            @Override
            public String toString() {
                return String.format("[%s, %s]", q.toString(), p.toString());
            }
        }

    }
}
