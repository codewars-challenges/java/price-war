package be.hics.sandbox.pricewar;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertTrue;

public class PriceWarKataExampleTest {

    @Test
    public void example1() {
        // Given
        BigDecimal[][] demand = {
                {BigDecimal.valueOf(1100D), BigDecimal.valueOf(25.13D)},
                {BigDecimal.valueOf(1000D), BigDecimal.valueOf(23.10D)},
                {BigDecimal.valueOf(888D), BigDecimal.valueOf(19.19D)},
                {BigDecimal.valueOf(3000D), BigDecimal.valueOf(11.80D)}
        };
        BigDecimal[][] supply = {
                {BigDecimal.valueOf(5000D), BigDecimal.valueOf(17.00D)},
                {BigDecimal.valueOf(2000D), BigDecimal.valueOf(16.00D)}
        };
        BigDecimal capacity = BigDecimal.valueOf(500D);
        BigDecimal result = PriceWar.pickYourPrice(demand, supply, capacity);
        assertTrue(result.compareTo(BigDecimal.valueOf(16.99D)) == 0);
    }

    @Test
    public void example2() {
        // Given
        BigDecimal[][] demand = {
                {BigDecimal.valueOf(100D), BigDecimal.valueOf(20.00D)},
                {BigDecimal.valueOf(50D), BigDecimal.valueOf(30.00D)},
                {BigDecimal.valueOf(300D), BigDecimal.valueOf(5.00D)},
                {BigDecimal.valueOf(5000D), BigDecimal.valueOf(25.00D)}
        };
        BigDecimal[][] supply = {
                {BigDecimal.valueOf(1000D), BigDecimal.valueOf(27.00D)},
                {BigDecimal.valueOf(500D), BigDecimal.valueOf(16.00D)}
        };
        BigDecimal capacity = BigDecimal.valueOf(250D);
        BigDecimal result = PriceWar.pickYourPrice(demand, supply, capacity);
        assertTrue(result.compareTo(BigDecimal.valueOf(25.00D)) == 0);
    }

    @Test
    public void example3() {
        // Given
        BigDecimal[][] demand = {
                {BigDecimal.valueOf(1000D), BigDecimal.valueOf(10.00D)},
                {BigDecimal.valueOf(1000D), BigDecimal.valueOf(11.00D)},
                {BigDecimal.valueOf(1000D), BigDecimal.valueOf(4.80D)},
                {BigDecimal.valueOf(400D), BigDecimal.valueOf(12.00D)}
        };
        BigDecimal[][] supply = {
                {BigDecimal.valueOf(1500D), BigDecimal.valueOf(4.75D)},
                {BigDecimal.valueOf(1500D), BigDecimal.valueOf(4.75D)}
        };
        BigDecimal capacity = BigDecimal.valueOf(900D);
        BigDecimal result = PriceWar.pickYourPrice(demand, supply, capacity);
        assertTrue(result.compareTo(BigDecimal.valueOf(12.00D)) == 0);
    }

}
